export interface Playlist{
  id: number;
  name: string;
  favourite: boolean;
  color: string;
  /**
    List of Track Objects
  */
  tracks?: Tracks;
}

type Tracks = Track[]

interface Track{

}
