import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core'
import { Playlist } from './playlist';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styles: [`
    .list-group-item{
      border-left: 10px solid transparent;
    }  
  `],
  encapsulation: ViewEncapsulation.Emulated,
  // inputs:['playlists:items']
})
export class ItemsComponent implements OnInit {

  hover:Playlist

  @Input()
  selected:Playlist

  @Output()
  selectedChange = new EventEmitter<Playlist>()

  select(playlist){
    this.selectedChange.emit(playlist)
    // this.selected = playlist
  }

  @Input('items')
  playlists: Playlist[]

  trackFn(index,item){
    return item.id
  }

  constructor() { }

  ngOnInit() {
  }

}
