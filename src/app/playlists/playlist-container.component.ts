import { Component, OnInit } from '@angular/core';
import { PlaylistsService } from './playlists.service';
import { Playlist } from './playlist'
import { PlaylistsComponent } from './playlists.component';
import { ActivatedRoute } from '@angular/router'
import { map, pluck, switchMap } from 'rxjs/operators'

@Component({
  selector: 'app-playlist-container',
  templateUrl: './playlist-container.component.html',
  styles: []
})
export class PlaylistContainerComponent implements OnInit {
  
  selected$
  selected:Playlist

  constructor(
    private playlistsService: PlaylistsService,
    private route: ActivatedRoute) {

    this.selected$ = this.route.parent.data
    .pipe(
      pluck<any,Playlist>('selected')
    )
  }

  ngOnInit() {
    
  }

  save(playlist) {
    this.selected$ = this.playlistsService.savePlaylist(playlist)
  }


}
