import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { async, inject, TestBed, fakeAsync, tick } from '@angular/core/testing'

import { PlaylistsService } from './playlists.service';
import { Playlist } from './playlist';
import { buffer, bufferCount, bufferToggle, mapTo, merge } from 'rxjs/operators'
import { Subject } from 'rxjs/Subject';
import { bufferWhen } from 'rxjs/operators/bufferWhen';

fdescribe('PlaylistsService', () => {
  let playlistsService: PlaylistsService,
    backend: HttpTestingController,
    fixture: Playlist[]

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        PlaylistsService
      ]
    });
    fixture = [
      {
        "id": 1,
        "name": "Angular TOP 20",
        "color": "#00ff00",
        "favourite": false
      },
      {
        "id": 2,
        "name": "Angular Greatest Hits",
        "color": "#0000ff",
        "favourite": false
      },
      {
        "id": 3,
        "name": "Angular The Best of",
        "color": "#ff0000",
        "favourite": true
      }
    ]
  });

  beforeAll(() => {
    // jasmine.clock().install()
  })

  beforeEach(inject([PlaylistsService, HttpTestingController],
    (
      service: PlaylistsService,
      backendController: HttpTestingController) => {

      playlistsService = service
      backend = backendController
    }));

  it('should be created', () => {
    expect(playlistsService).toBeTruthy();
  });

  it('should async get all playlists ', (() => {
    playlistsService.getPlaylists()
      .subscribe(playlists => {
        expect(playlists).toEqual(fixture)
      })

    backend.expectOne('http://localhost:3000/playlists/', 'GET /playlists').flush(fixture, {})
  }))


  it('should async get one playlist by id', () => {
    playlistsService.getPlaylist(fixture[0].id)
      .subscribe(playlist => {
        expect(playlist).toEqual(fixture[0])
      })

    backend.expectOne('http://localhost:3000/playlists/1', 'GET /playlist/1').flush(fixture[0], {})
  })

  it('should async save playlist', () => {
    let changedPlaylist = { ...fixture[0] } // Object.assign({},fixture)
    changedPlaylist.name = 'changed'

    let save$ = playlistsService.savePlaylist(changedPlaylist)

    save$.subscribe((playlist) => {
          expect(playlist).toEqual(changedPlaylist)
    })

    backend.expectOne({
      url: 'http://localhost:3000/playlists/1',
      method: 'PUT'
    }, 'PUT playlists/1').flush(changedPlaylist, {})
  })


  xit('should emit updated playlists on save', fakeAsync(() => {
    let changedPlaylist = { ...fixture[0] } // Object.assign({},fixture)
    changedPlaylist.name = 'changed'

    let events$ = new Subject(),
      flush$ = new Subject()

    let events = []

    events$
      .pipe(
      bufferWhen(() => flush$)
      )
      .subscribe(e => events = e)

    playlistsService
      .getPlaylists()
      .subscribe(e => {
        events$.next('get ' + e[0].name)
      })

    playlistsService
      .savePlaylist(changedPlaylist)
      .subscribe(e => {
        tick(201)
        events$.next('save ' + e.name)
      })

    flush$.next()
    expect(events).toEqual([
      'get ' + fixture[0].name,
      'get changed',
      'save changed'
    ])

  }))


});
