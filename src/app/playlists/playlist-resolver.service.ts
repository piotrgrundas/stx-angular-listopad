import { Playlist } from './playlist'
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router'
import { PlaylistsService } from './playlists.service';

@Injectable()
export class PlaylistResolverService implements Resolve<Playlist>{

  constructor(private playlistsService:PlaylistsService) { }


  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    let id = route.paramMap.get('playlist_id')
    if(!id){
      id = route.firstChild.paramMap.get('playlist_id')
    }
  
    return id? this.playlistsService
    .getPlaylist(parseInt(id)) : null
  }
}
