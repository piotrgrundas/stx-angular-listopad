import { HttpClient } from '@angular/common/http'
import { delay, mapTo, tap } from 'rxjs/operators'
import { Injectable } from '@angular/core';
import { Playlist } from './playlist';
import { of } from 'rxjs/observable/of';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class PlaylistsService{

  playlists$ = new BehaviorSubject([])

  constructor(private http: HttpClient) { }

  getPlaylists() {
    this.fetchPlaylists()
    return this.playlists$.asObservable()
  }

  fetchPlaylists(){
    this.http.get<Playlist[]>('http://localhost:3000/playlists/')
    .subscribe(playlists => this.playlists$.next(playlists))
  }

  getPlaylist(id) {
    return this.http.get<Playlist>('http://localhost:3000/playlists/' + id)
  }

  savePlaylist(playlist) {
    return this.http.put<Playlist>('http://localhost:3000/playlists/' + playlist.id, playlist).pipe(tap(
      () => this.fetchPlaylists()
    ))
  }
}
