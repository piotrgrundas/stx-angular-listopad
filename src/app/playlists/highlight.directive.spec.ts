import { HighlightDirective } from './highlight.directive';

describe('HighlightDirective', () => {
  let directive:HighlightDirective;

  beforeEach(()=>{
    directive = new HighlightDirective();
    directive.ngOnInit()
  })

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should have no color',()=>{
    expect(directive.color).toEqual('transparent')
  })

  it('should have color when hovered',()=>{
    directive.hoverIn()
    directive.setColor = 'red'
    expect(directive.color).toEqual('red')
  })

  it('should have transparent when not hovered',()=>{
    directive.hoverIn()
    directive.setColor = 'red'
    expect(directive.color).toEqual('red')
    directive.hoverOut()
    expect(directive.color).toEqual('transparent')
  })

});
