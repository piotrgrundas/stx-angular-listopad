import { BrowserModule } from '@angular/platform-browser';
import { Inject, NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import { DropdownComponent } from './ui/dropdown.component';
import { DropdownMenuDirective } from './ui/dropdown-menu.directive';
import { DropdownToggleDirective } from './ui/dropdown-toggle.directive';
import { DropdownDirective } from './ui/dropdown.directive';
import { MusicModule } from './music/music.module';
import { environment } from '../environments/environment'
import { API_URL } from './music/music.service';
import { AuthService } from './auth/auth.service';
import { HTTP_INTERCEPTORS, HttpClientXsrfModule, HttpXsrfTokenExtractor } from '@angular/common/http';
import { AuthInterceptorService } from './auth/auth-interceptor.service';
import { Routing } from './app.routing';
import { PlaylistsModule } from './playlists/playlists.module';

@NgModule({
  declarations: [
    AppComponent,
    DropdownComponent,
    DropdownMenuDirective,
    DropdownToggleDirective,
    DropdownDirective
  ],
  imports: [
    BrowserModule,
    PlaylistsModule,
    MusicModule,
    // HttpClientXsrfModule.withOptions({
    //   headerName:'Authorization'
    // }),
    Routing
  ],
  providers: [
    AuthService,
    // { provide: HttpXsrfTokenExtractor, useExisting: AuthService },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true },
    { provide: API_URL, useValue: environment.api_url },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private auth: AuthService) {
    this.auth.getToken()
  }
}
