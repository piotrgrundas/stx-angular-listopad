import { HTTP_INTERCEPTORS } from '@angular/common/http'
import { Inject, Injectable } from '@angular/core'

@Injectable()
export class AuthService {

  token

  constructor() { }

  authorize() {
    sessionStorage.removeItem('token')

    let url = 'https://accounts.spotify.com/authorize',
      client_id = '43d2d2d7ebed49a6b8b837b51bc624b6',
      response_type = 'token',
      redirect_uri = 'http://localhost:4200/'

    url = `${url}?client_id=${client_id}&response_type=${response_type}&redirect_uri=${redirect_uri}`

    location.replace(url)
  }

  getToken() {
    this.token = JSON.parse(sessionStorage.getItem('token'))

    if (!this.token) {
      let match = location.hash.match(/access_token=([^&]*)/)
      this.token = match && match[1]
      location.hash = ''
    }

    if (!this.token) {
      this.authorize()
    }
    sessionStorage.setItem('token', JSON.stringify(this.token))

    return this.token
  }

}
