import { catchError, retry, retryWhen } from 'rxjs/operators'
import { Injectable } from '@angular/core';

import { HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http'
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler) {

    let request = req.clone({
      headers: req.headers.append('Authorization', `Bearer ${this.auth.getToken()}`)
    })

    return next.handle(request)
      .pipe(
        catchError((err, caught) => {
          if (err.statusText == "Unauthorized") {
            // throw new Error('Server error')
            this.auth.authorize()
          } else {
            console.error(err)
          }
          return []
        })
      )
  }

  constructor(private auth: AuthService) { }

}
