import { Directive, EventEmitter, HostListener } from '@angular/core'
//  (click)="toggle()"

@Directive({
  selector: '[appDropdownToggle]'
})
export class DropdownToggleDirective {

  @HostListener('click')
  toggle(){
    this.toggled.emit()
  }

  toggled = new EventEmitter()

  constructor() { }

}
