import { DebugElement } from '@angular/core'
import { By } from '@angular/platform-browser'
import { DropdownComponent } from './dropdown.component'
import { ComponentFixture, TestBed } from '@angular/core/testing'
import { DropdownMenuDirective } from './dropdown-menu.directive';

describe('DropdownComponent', () => {
  let fixture: ComponentFixture<DropdownComponent>
  let component: DropdownComponent
  let getMenu = (): DebugElement => {
    return fixture.debugElement.query(By.css('.dropdown-menu'))
  }
  let getToggler = () => {
    return fixture.debugElement.query(By.css('.dropdown-toggle'))
  }
  let getDropdown = () => {
    return fixture.debugElement.query(By.css('.dropdown'))
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        DropdownComponent,
        DropdownMenuDirective
      ],
      imports: [],
      providers: [],
      schemas: []
    })
      .compileComponents()
    fixture = TestBed.createComponent(DropdownComponent)
    component = fixture.componentInstance
    
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(fixture.componentInstance).toBeTruthy()
  })

  it('should have closed menu on start', () => {
    let elem = getMenu()
    expect(elem && elem.nativeElement).toBeNull()
  })

  it('should open menu', () => {
    component.open()
    fixture.detectChanges()
    let elem = getMenu()
    expect(elem && elem.nativeElement).not.toBeNull('.dropdown-menu should be opened')
  })

  it('should close menu', () => {
    // Given
    let elem = getMenu()
    component.open()
    fixture.detectChanges()

    // When
    component.close()
    fixture.detectChanges()

    // Then
    expect(elem && elem.nativeElement).toBeNull('.dropdown-menu should be closed')
  })

  it('should toggle menu on click', () => {
    expect(component.opened).toBeFalsy()
    let button = getToggler()
    button.triggerEventHandler('click', {})
    expect(component.opened).toBeTruthy()
  })

  it('should toggle css class on toggle', () => {
    let menu, dropdown = getDropdown()
    let expectedClasses = jasmine.objectContaining({ show: true })

    // show
    component.open()
    fixture.detectChanges()
    menu = getMenu()
    expect(menu.classes).toEqual(expectedClasses)
    expect(dropdown.classes).toEqual(expectedClasses)

    // hide
    component.close()
    fixture.detectChanges()
    expect(dropdown.classes).not.toEqual(expectedClasses)
  })

})