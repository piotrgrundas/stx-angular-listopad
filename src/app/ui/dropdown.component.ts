import { DropdownToggleDirective } from './dropdown-toggle.directive'
import { Component, ContentChild, OnInit, TemplateRef, ViewChild } from '@angular/core'
import { DropdownMenuDirective } from './dropdown-menu.directive';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styles: []
})
export class DropdownComponent implements OnInit {

  @ContentChild(DropdownMenuDirective
    /* , { read: DropdownMenuDirective } */
  )
  menuRef:DropdownMenuDirective

  @ContentChild(DropdownToggleDirective)
  toggleRef: DropdownToggleDirective

  // After all view directives had their ngOnInit
  ngAfterContentInit() {

    this.toggleRef.toggled.subscribe(()=>{
      this.toggle()
    })
  }

  opened = false

  open() {
    this.opened = true
    this.menuRef.setOpened(this.opened)
  }

  close() {
    this.opened = false
    this.menuRef.setOpened(this.opened)
  }

  toggle() {
    this.opened = !this.opened
    this.menuRef.setOpened(this.opened)
  }

  constructor() { }

  ngOnInit() {
  }

}
