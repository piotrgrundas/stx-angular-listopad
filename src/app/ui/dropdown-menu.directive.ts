import { Directive, HostBinding, Input, TemplateRef, ViewContainerRef, ViewRef } from '@angular/core'

@Directive({
  selector: '[appDropdownMenu]'
})
export class DropdownMenuDirective {

  constructor(
    // private tpl: TemplateRef<any>,
    // private vcr: ViewContainerRef  
  ) { }

  viewCache: ViewRef

  @Input('open')
  @HostBinding('class.show')
  isOpen
  
  setOpened(open) {
    this.isOpen = open
  }
}
