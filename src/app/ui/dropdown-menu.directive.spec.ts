import { By } from '@angular/platform-browser'
import { Component, DebugElement, DebugNode} from '@angular/core'
import { ComponentFixture, TestBed, async } from '@angular/core/testing'
import { DropdownMenuDirective } from './dropdown-menu.directive';

@Component({
  template: `<div *appDropdownMenu="show">Test</div>`
})
export class TestingComponent { 
  show = false
}

describe('DropdownMenuDirective', () => {
  let fixture: ComponentFixture<TestingComponent>,
    debugElem: DebugElement
  let getElem = () => fixture.debugElement.children[0]

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        TestingComponent,
        DropdownMenuDirective
      ]
    }).compileComponents()
    fixture = TestBed.createComponent(TestingComponent)
    debugElem = getElem()
  })

  it('should be closed', () => {
    expect(debugElem).not.toBeTruthy();
  });

  it('should open', () => {
    fixture.debugElement.componentInstance.show = true
    fixture.detectChanges()
    let elem = getElem()
    expect(elem && elem.nativeElement).toBeTruthy()
  });

  it('should close', (done) => {
    fixture.debugElement.componentInstance.show = true
    fixture.detectChanges()
    fixture.debugElement.componentInstance.show = false
    fixture.detectChanges()
    let elem = getElem()
    expect(elem && elem.nativeElement).not.toBeTruthy()

  });
});


    // setTimeout(()=>{
    //   console.log(fixture.debugElement.query(By.directive(DropdownMenuDirective)))
    //   done()
    // },100)

