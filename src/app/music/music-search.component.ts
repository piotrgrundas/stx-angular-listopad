import { Component, OnInit } from '@angular/core';
import { MusicService } from './music.service';

@Component({
  selector: 'app-music-search',
  templateUrl: './music-search.component.html',
  styles: [],
  // viewProviders:[
  //   MusicService
  // ]
})
export class MusicSearchComponent implements OnInit {

  albums$
  query$

  constructor(private musicService:MusicService) {
    this.albums$ = this.musicService.getAlbums()  
    this.query$ = this.musicService.getQuery()  
   }

  ngOnInit() {
  }

  search(query) {
    this.musicService.search(query)
  }

}
