interface Entity { id: string }
interface Named { name: string }

export interface Album  extends Entity, Named{
  images: AlbumImage[];
  artists: Artist[]
}

export interface AlbumImage{
  url:string;
  width: number;
  height: number;
}

export interface Artist extends Entity, Named{}



// https://developer.spotify.com/web-api/object-model/#album-object-simplified
