import { RouterModule, Routes } from '@angular/router'
import { MusicSearchComponent } from './music-search.component';

export const routes:Routes = [
  { path: 'music', component: MusicSearchComponent },
]

export const Routing = RouterModule.forChild(routes)