import { MusicService, API_URL } from './music.service'
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicSearchComponent } from './music-search.component';
import { SearchFormComponent } from './search-form.component';
import { AlbumsListComponent } from './albums-list.component';
import { AlbumItemComponent } from './album-item.component';
import { ReactiveFormsModule} from '@angular/forms'

import { HttpClientModule } from '@angular/common/http';
import { MusicContainerComponent } from './music-container.component'
import { Routing  } from './music.routing';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    Routing
  ],
  declarations: [
    MusicSearchComponent, 
    SearchFormComponent,
    AlbumsListComponent, 
    AlbumItemComponent, MusicContainerComponent
  ],
  providers:[
    { provide: API_URL, useValue: 'http://localhost/'},
    MusicService
  ],
  exports:[
    // MusicSearchComponent,
    // MusicContainerComponent
  ]
})
export class MusicModule { }
