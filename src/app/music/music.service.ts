import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { EventEmitter, Inject, Injectable, InjectionToken } from '@angular/core'
import { Album } from './album';
import { AuthService } from '../auth/auth.service';

export const API_URL = new InjectionToken('Spotify Search Url')

// import 'rxjs/Rx'
// import 'rxjs/add/operator/map'
import { catchError, distinctUntilChanged, filter, map, pluck, startWith, switchMap, tap } from 'rxjs/operators'
import { of } from 'rxjs/observable/of'
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject'

interface AlbumsResponse {
  albums: {
    items: Album[]
  }
}

@Injectable()
export class MusicService {

  albums: Album[] = []

  albums$ = new BehaviorSubject<Album[]>(this.albums)

  query$ = new BehaviorSubject<string>('')

  constructor( @Inject(API_URL) private api_url,
    private http: HttpClient) {

    this.query$.pipe(
      // tap( q => console.log(q) ),
      filter((query: string) => query.length >= 3),
      distinctUntilChanged(),
      map(query => ({ q: query, type: 'album' })),
      switchMap(
        params => this.http.get<AlbumsResponse>(this.api_url, {
          params
        })
      ),
      map(response => response.albums.items),
    )
      .subscribe(albums => {
        this.albums$.next(albums)
      })
  }

  search(query) {
    this.query$.next(query)
  }

  getAlbums() {
    return this.albums$.asObservable()
  }

  getQuery() {
    return this.query$.asObservable()
  }

}
