import { PartialObserver } from 'rxjs/Observer'
import { debounceTime, distinct, distinctUntilChanged, filter, mapTo, tap, withLatestFrom } from 'rxjs/operators'
import { Component, Input, OnInit, Output } from '@angular/core'
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators, ValidationErrors, AsyncValidatorFn } from '@angular/forms'
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styles: [`
    form .ng-invalid.ng-touched,
    form .ng-invalid.ng-dirty {
      border: 2px solid red !important;
    }
  `]
})
export class SearchFormComponent implements OnInit {

  queryForm: FormGroup

  @Input('query')
  set query(query) {
    this.queryForm.get(['search', 'query']).setValue(query,{
      onlySelf: true,
      emitEvent:false
    })
  }

  @Output()
  search

  queryField: FormControl

  constructor(private builder: FormBuilder) {

    const censor = (badword: string): ValidatorFn =>
      (control: AbstractControl): ValidationErrors => {

        let hasError = control.value.indexOf(badword) !== -1

        return hasError ? {
          'censor': badword
        } : null
      }

    const asyncCensor = (badword: string): AsyncValidatorFn =>
      (control: AbstractControl) => {

        return Observable.create((observer: PartialObserver<ValidationErrors>) => {

          let resource = setTimeout(() => {
            observer.next(censor(badword)(control))
            observer.complete()
          }, 2000)

          // unsubscribe
          return () => { clearTimeout(resource) }
        })
      }

    this.queryField = this.builder.control('', [
      Validators.required,
      Validators.minLength(3),
      // censor('bat')
    ], [
        asyncCensor('bat')
      ])

    this.queryForm = this.builder.group({
      'search': this.builder.group({
        'query': this.queryField
      })
    })
    window['queryForm'] = this.queryForm

    const valid$ = this.queryField.statusChanges.pipe(
      // tap( q => console.log(q) ),
      filter(status => status == "VALID"),
      //mapTo(true)
    )
    
    const value$ = this.queryForm.get(['search', 'query']).valueChanges.pipe(
      debounceTime(400),
    )
    
    this.search = valid$.pipe(
      withLatestFrom(value$, (valid, value) => value)
    )
  }

  ngOnInit() {
  }
}
